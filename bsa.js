import Chat from './src/Chat';
import {rootReducer} from "./src/store/rootReducer";

export default {
    Chat,
    rootReducer,
};