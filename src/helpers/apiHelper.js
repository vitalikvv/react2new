
export function callApi(API_URL) {
    const url = API_URL;
    const options = {
        method: 'GET',
    };
    return fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .catch((error) => {
            throw error;
        });
}