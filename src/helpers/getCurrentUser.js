import { v4 as uuidv4 } from 'uuid';

const names = ['Mike', 'Beck', 'Dana', 'Bella', 'Joe', 'John', 'David']

const getRandomFoto = async () => {
    const url = 'https://source.unsplash.com/random/800x800/?face';
    const response = await fetch(url);
    return response.url
}

const getUserName = () => {
    return names[Math.floor(Math.random()*names.length)];
}

export async function getCurrentUser() {
    const avatar = await getRandomFoto();
    const user = getUserName();
    const userId = uuidv4();

    return {avatar, user, userId}
}