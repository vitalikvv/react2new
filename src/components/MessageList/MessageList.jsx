import React from 'react'
import {Container, Divider} from 'semantic-ui-react'
import './MessageList.css'
import Message from "../Message/Message";
import OwnMessage from "../OwnMessage/OwnMessage";
import {getMonth, getWeekDay} from "../../helpers/changeDateFormat";
import {connect} from "react-redux";
import {
    deleteMessage,
    editMessage,
    likeMessage,
    setLikeOrDis, setOpenModal,
    setOpenToast
} from "../../store/messagesList";


const MessageList = (props) => {

    let date = '';
    const now = new Date();
    const millisecondsInDay = 86400000;
    const lastIndex = props.messages.length - 1;

    return (
        <div className='message-list'>
            <Container text style={{marginTop: '7em'}}>
                {props.messages.map((message,i) => {

                        if (date !== message.createdAt.split(' ')[0]) {
                            date = message.createdAt.split(' ')[0]
                            const dateParts = message.createdAt.split(' ')[0].split('-');
                            const dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
                            const timeDiff = new Date(now) - new Date(dateObject);

                            let dateToDisplay = getWeekDay(new Date(dateObject)) + `, ${dateParts[0]} ` + getMonth(new Date(dateObject)) + ` ${dateParts[2]}`

                            if (timeDiff < millisecondsInDay) {
                                dateToDisplay = 'Today'
                            }

                            if (timeDiff >= millisecondsInDay && timeDiff < millisecondsInDay*2) {
                                dateToDisplay = 'Yesterday'
                            }

                            return (
                                <div key={message.id}>
                                    <Divider className='messages-divider' horizontal>{dateToDisplay}</Divider>
                                    {message.userId === props.currentUser.userId
                                        ? <OwnMessage
                                            likeMessage={props.likeMessage}
                                            setOpenToast={props.setOpenToast}
                                            setLikeOrDis={props.setLikeOrDis}
                                            setOpenModal={props.setOpenModal}
                                            isOpenModal={props.isOpenModal}
                                            message={message}
                                            deleteMessage={props.deleteMessage}
                                            updateMessage={props.editMessage}
                                            isLastElement={i === lastIndex}
                                        />
                                        : <Message
                                            message={message}
                                            likeMessage={props.likeMessage}
                                            setOpenToast={props.setOpenToast}
                                            setLikeOrDis={props.setLikeOrDis}
                                        />}
                                </div>
                            )
                        } else {

                            return (
                                <div key={message.id}>
                                    {message.userId === props.currentUser.userId
                                        ? <OwnMessage
                                            likeMessage={props.likeMessage}
                                            setOpenToast={props.setOpenToast}
                                            setLikeOrDis={props.setLikeOrDis}
                                            setOpenModal={props.setOpenModal}
                                            isOpenModal={props.isOpenModal}
                                            message={message}
                                            deleteMessage={props.deleteMessage}
                                            updateMessage={props.editMessage}
                                            isLastElement={i === lastIndex}
                                        />
                                        : <Message
                                            message={message}
                                            likeMessage={props.likeMessage}
                                            setLikeOrDis={props.setLikeOrDis}
                                            setOpenToast={props.setOpenToast}
                                        />}
                                </div>
                            )
                        }
                    }
                )}
            </Container>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        messages: state.chat.messages,
        currentUser: state.chat.currentUser,
        isOpenModal: state.chat.isOpenModal
    }
}

const MessageListWithState = connect(mapStateToProps,
    { likeMessage, setOpenToast, setLikeOrDis, setOpenModal, editMessage, deleteMessage })(MessageList)

export default MessageListWithState