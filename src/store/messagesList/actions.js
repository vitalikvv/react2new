export const messagesListActionTypes = {
    SET_MESSAGES: "MESSAGESLIST.SET_MESSAGES",
    SET_CURRENT_USER: "MESSAGESLIST.SET_CURRENT_USER",
    ADD_MESSAGE: "MESSAGESLIST.ADD_MESSAGE",
    EDIT_MESSAGE: "MESSAGESLIST.EDIT_MESSAGE",
    LIKE_MESSAGE: "MESSAGESLIST.LIKE_MESSAGE",
    DELETE_MESSAGE: "MESSAGESLIST.DELETE_MESSAGE",
    SET_START_LOADING: "MESSAGESLIST.SET_START_LOADING",
    SET_OPEN_MODAL: "MESSAGESLIST.SET_OPEN_MODAL",
    SET_LIKE_OR_DIS: "MESSAGESLIST.LIKE_OR_DIS",
    SET_OPEN_TOAST: "MESSAGESLIST.SET_OPEN_TOAST"
}

export const setMessages = (messages) => {
    return ({
        type: messagesListActionTypes.SET_MESSAGES,
        payload: messages
    })
}

export const setCurrentUser = (user) => {
    return ({
        type: messagesListActionTypes.SET_CURRENT_USER,
        payload: user
    })
}

export const setOpenModal = (isOpen) => {
    return ({
        type:messagesListActionTypes.SET_OPEN_MODAL,
        payload:isOpen
    })
}

export const addMessage = (text, currentUser) => {
    const {userId, avatar, user} = currentUser;
    const now = new Date();
    const year = now.getFullYear()
    const month = `${now.getMonth() + 1}`.padStart(2, 0)
    const day = `${now.getDate()}`.padStart(2, 0)
    const hh = ('0' + now.getHours()).slice(-2);
    const min = ('0' + now.getMinutes()).slice(-2);
    const date = [day, month, year].join("-") + ' ' + hh + ':' + min

    const newMessage = {
        text: text,
        editedAt: '',
        id: String(now),
        createdAt: date,
        likesCount: 0,
        userId,
        avatar,
        user
    }
    return ({
        type: messagesListActionTypes.ADD_MESSAGE,
        payload: newMessage
    })
}

export const editMessage = (newMessageText, messageId) => ({
    type: messagesListActionTypes.EDIT_MESSAGE,
    payload: {
        newMessageText,
        messageId
    }
})

export const likeMessage = (messageId, likeOrDis) => {
    return ({
        type: messagesListActionTypes.LIKE_MESSAGE,
        payload: {
            messageId,
            likeOrDis: likeOrDis
        }
    })
}

export const deleteMessage = (messageId) => ({
    type: messagesListActionTypes.DELETE_MESSAGE,
    payload: messageId
})

export const setStartLoading = (isLoading) => ({
    type:messagesListActionTypes.SET_START_LOADING,
    payload:isLoading
})

export const setLikeOrDis = (likeOrDis) => ({
    type:messagesListActionTypes.SET_LIKE_OR_DIS,
    payload:likeOrDis
})

export const setOpenToast = (isOpen) => {
    return ({
        type:messagesListActionTypes.SET_OPEN_TOAST,
        payload:isOpen
    })
}